# PURPOSE: gitlab-ci pipeline definitions (common to all builds)
# see https://docs.gitlab.com/ce/ci/yaml/README.html for all available options


# IMPORT COMMON DEFINITIONS
# =========================
include:
  # enable secret detection
  - template: Security/Secret-Detection.gitlab-ci.yml
  # enable builtin SAST jobs
  - template: Jobs/SAST.gitlab-ci.yml




# global CI variables
# ===================
variables:
  # version of pipeline-definition
  PIPELINE_DEFINITIONS_VERSION: "2023-08-21-001"

  # disable ssl certs checking in git
  GIT_SSL_NO_VERIFY: "true"

  # project build settings
  APP_NAME:    "${CI_PROJECT_NAME}"
  APP_PATH:    "${CI_PROJECT_PATH}"
  APP_VERSION: "${CI_COMMIT_REF_NAME}"




# list of build stages
# ====================
stages:
  - check
  - build
  - tests
  - publish
  - deploy
  - cleanup




# common pre-tasks
# ================
before_script:
  - env|sort >build.env         # save current environment to file
  - ls -la ${CI_PROJECT_DIR}/   # show content of build path
    # debugging some envvars
  - |
      echo "[before_script] PIPELINE_DEFINITIONS_VERSION=${PIPELINE_DEFINITIONS_VERSION}"
      echo "[before_script] REGISTRY=${REGISTRY_USER}/$(echo ${REGISTRY_PASS}|sed 's#.*\(...\)$#****\1#')@${REGISTRY_HOST}"
      echo "[before_script] REPOSITORY=${REPOSITORY_USER}/$(echo ${REPOSITORY_PASS}|sed 's#.*\(...\)$#****\1#')@${REPOSITORY_HOST}"
      echo "[before_script] THIS_HOST=${HOSTNAME}"
      echo "[before_script] THIS_USER=$(whoami 2>/dev/null || echo ${USER:-unknown})"
  # load custom CICD environment file if set
  - |
      if [ -f ${CI_PROJECT_DIR}/cicd.env ];then 
        echo "[before_script] loading custom CICD envvars from ${CI_PROJECT_DIR}/cicd.env"
        source ${CI_PROJECT_DIR}/cicd.env
      fi  




# BUILD JOB DEFINITIONS
# =====================

# -----------------------------------------
# override settings for imported SAST jobs
# -----------------------------------------
sast:
  stage: check

.sast-analyzer:
  stage: check

.secret-analyzer:
  stage: check


# ------------------------------------------------------------------------------
# deploy kb contents to gitlab page using gitbook
# ------------------------------------------------------------------------------
pages:
  artifacts:
    paths:
      - ./build.*
      - public
    expire_in: 1 week
  needs:
    - job: secret_detection
      artifacts: false
  image: "python:3-slim"
  only:
    - master
  script:
  - cd doc                                                              # move to the doc sources path
  - pip install -r requirements.txt                                     # install all pip requirements
  - mkdocs build --strict --verbose --site-dir ${CI_PROJECT_DIR}/public # build to public path
  - find ${CI_PROJECT_DIR}/public                                       # show generated files
  stage: deploy
  tags:
    - iops
    - docker


# eof