#!/usr/bin/env python3
# simple app to serve local html files
import http.server
import socketserver

# options
PORT = 8080
Handler = http.server.SimpleHTTPRequestHandler

# start servers
with socketserver.TCPServer(("", PORT), Handler) as httpd:
    print("[i] serving at port", repr(PORT))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        print ("[i] shutting down...")

# eof
