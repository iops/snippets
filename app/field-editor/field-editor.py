#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
custom note-editor service for managing single note field from a db table
"""
VERSION = '0.0.1'

# import required standard modules
import os, re, sys
from datetime import datetime
from types import ModuleType
from typing import Dict, List, Optional

# import custom modules (must be installed using pip)
import bottle               # web framework
import loguru               # logging framework




# ------------------------------------------------------------------------------
# app: main settings (overwriteable by environment variables)
# ------------------------------------------------------------------------------
app_settings = {
    # application runtime properties
    'app_name': os.environ.get('APP_NAME', os.path.basename(__file__).split('.')[0]),
    'app_host': os.environ.get('APP_HOST', 'localhost'),
    'app_port': os.environ.get('APP_PORT', 8000),
    'app_version': os.environ.get('APP_VERSION', VERSION),
    'context_root': os.environ.get('CONTEXT_ROOT', ''),
    # otrs database connection settings (defaults to local connection)
    'db_host': os.environ.get('DB_HOST', 'localhost'),
    'db_port': os.environ.get('DB_PORT', 5432),
    'db_user': os.environ.get('DB_USER', 'otrs'),
    'db_pass': os.environ.get('DB_PASS', 'otrs'),
    'db_name': os.environ.get('DB_NAME', 'otrs'),
    'db_type': os.environ.get('DB_TYPE', 'postgresql'),
    # development settings
    'dev_mode': os.environ.get('DEV_MODE', False),
}
# safe version without sensitive data (for displaying purposes)
app_settings_safe = { key:value if (not re.search(r'(key|token|pass)', key, re.IGNORECASE)) else '***' for (key,value) in app_settings.items() }


# ------------------------------------------------------------------------------
# sql: list of support fields (refered by object_name)
# ------------------------------------------------------------------------------
sql_fields = {
    # ===============
    'REALAX_PRD': {
    # ===============
        'table_name': 'datasource_renters_realax',      # db table name
        'table_pkey': 'vertragsnummer',                 # primary/unique key for identifying a record
        'field_name': 'custom_notes',                   # name of the managed field
        # list of additional fields to display (optional)
        'infos_fields': ['alle_mieter_suchname','objekt_id','strasse','ort','updated_date'],
    },
    # ===============
    'SUPREME_MAIN': {
    # ===============
        'table_name': 'datasource_renters_supreme',     # db table name
        'table_pkey': 'vertragsnummer',                 # primary/unique key for identifying a record
        'field_name': 'custom_notes',                   # name of the managed field
        # list of additional fields to display (optional)
        'infos_fields': ['alle_mieter_suchname','objekt_id','strasse','ort','updated_date'],
    },
    # ===============
    'SUPREME_LEERSTAND': {
    # ===============
        'table_name': 'datasource_renters_supreme',     # db table name
        'table_pkey': 'vertragsnummer',                 # primary/unique key for identifying a record
        'field_name': 'custom_notes',                   # name of the managed field
        # list of additional fields to display (optional)
        'infos_fields': ['alle_mieter_suchname','objekt_id','strasse','ort','updated_date'],
    },
}



# ------------------------------------------------------------------------------
# helpers: exit with an error message
# ------------------------------------------------------------------------------
def exit_error(msg: str, exit_code: int = 1):
    """
        exit application with a custom error message
    """    
    loguru.logger.critical(msg)
    sys.exit(exit_code)


# ------------------------------------------------------------------------------
# helpers: return an html error message
# ------------------------------------------------------------------------------
def html_error(msg: str, status_class:str = 'danger'):
    """
        show an html custom error message
    """    
    return render_html_template(object_name='', object_id='', status_class=status_class, status_message=msg)



# ------------------------------------------------------------------------------
# helpers: string translation
# ------------------------------------------------------------------------------
def _translate(msg:str) -> str:
    """
        string translation wrapper
    """
    # does nothing at the moment
    return msg


# ------------------------------------------------------------------------------
# helpers: render html template
# ------------------------------------------------------------------------------
def render_html_template(
    # object name
    object_id:str,
    object_name:str,
    # field to edit
    field_name:str          = None,   
    field_value:str         = None,
    # additional infos to display (optional)
    field_infos:List[dict]   = None,
    # page options (optional)
    page_title:str =        app_settings['app_name'],
    page_description:str =  None,
    # status messages (optional)
    status_class:str =      'secondary',
    status_message:str =    None,
    # path to the html template file (optional, computed from filename by default)
    template_path:str =     None, 
    ) -> str:
    """
        render html template using provided parameters
    """
    # compute optional values if not set
    # ==================================
    # template path
    if not template_path:
        template_path = __file__.replace('.py','.html')
    # page description
    if not page_description:
        page_description = f"{_translate('Bearbeitung von')}: {object_name}/{object_id}/{field_name if field_name else '-'}"
    # get list of existing files if required
    # ======================================
    # compute template vars
    # =====================
    template_vars = {
        '_translate':               _translate,
        'app_settings':             app_settings,
        'object_id':                object_id,
        'object_name':              object_name,
        'field_name':               field_name,
        'field_value':              field_value,
        'field_infos':              field_infos,
        'page_title':               page_title,
        'page_description':         page_description,
        'status_class':             status_class,
        'status_message':           status_message,
    }
    # render template 
    # ===============
    return bottle.template(template_path, **template_vars)




# -----------------------------------------------------------------------------
# database: setup sql database connection
# -----------------------------------------------------------------------------
def sql_get_connection(db_settings: dict = None) -> object:
    """
        setup a database connection using the provided settings.
        - returns the connection object after a successful connection
        - exits on any error
        @param db_settings: the database settings
    """

    # use local settings by default
    # =============================
    if not db_settings:
        db_settings = {
            'db_type': app_settings['db_type'],
            'db_host': app_settings['db_host'],
            'db_port': app_settings['db_port'],
            'db_user': app_settings['db_user'],
            'db_pass': app_settings['db_pass'],
            'db_name': app_settings['db_name'],
        }

    # validate settings
    # =================
    for (key,value) in db_settings.items():
        if not value:
            exit_error(f"MISSING DB SETTING: {key}", exit_code=1)
    # set current context
    current_context = f"sql.{db_settings['db_type']}@{db_settings['db_host']}:{db_settings['db_port']}"

    # ORACLE
    # ======
    if db_settings['db_type'] in ['oracle']:
        db_connection_string = f"{db_settings['db_host']}:{db_settings['db_port']}/{db_settings['db_name']}"
        loguru.logger.info(f"[{current_context}] connecting to database")
        try:
            # import the driver
            import oracledb
            # create a connection to the database
            db_connection = oracledb.connect(
                user=db_settings['db_user'], 
                password=db_settings['db_pass'], 
                dsn=db_connection_string
            )
        except Exception as e:
            exit_error(f'[{current_context}] failed to connect to database: {e}')
        else:
            loguru.logger.success(f"[{current_context}] connected to {db_settings['db_type']}-database, version: {db_connection.version}")
            return db_connection

    # POSTGRESQL
    # ==========
    if db_settings['db_type'] in ['postgres','postgresql']:
        db_connection_string = f"{db_settings['db_host']}:{db_settings['db_port']}/{db_settings['db_name']}"
        loguru.logger.info(f"[{current_context}] connecting to database")
        try:
            # import the driver
            import psycopg2
            # create a connection to the database
            db_connection = psycopg2.connect(
                user=db_settings['db_user'], 
                password=db_settings['db_pass'], 
                host=db_settings['db_host'], 
                port=db_settings['db_port'], 
                database=db_settings['db_name'], 
            )
        except Exception as e:
            exit_error(f'[{current_context}] failed to connect to database: {e}')
        else:
            loguru.logger.success(f"[{current_context}] connected to {db_settings['db_type']}-database, version: {db_connection.server_version}")
            return db_connection

    # UNSUPPORTED
    # ===========
    else:
        exit_error(f'[{current_context}] Unsupported database type {db_settings["db_type"]}')


# -----------------------------------------------------------------------------
# database: get field values from database
# -----------------------------------------------------------------------------
def sql_get_field(object_name:str, object_id: int) -> dict:
    """ get value of defined field from database """
    # skip unsupported objects
    if object_name not in sql_fields.keys():
        exit_error(f"get_existing_items: unsupported object_name: {object_name}")
    # get options from sql_fields definitions
    table_name = sql_fields[object_name]['table_name']
    field_name = sql_fields[object_name]['field_name']
    field_list =  [ sql_fields[object_name]['table_pkey'] ]
    field_list += [ sql_fields[object_name]['field_name'] ]
    field_list += sql_fields[object_name]['infos_fields']
    # get entry from database
    current_context = f"sql_get_field.{object_name}.{object_id}.{field_name}"
    try:
        loguru.logger.info(f"[{current_context}] get value from table {table_name}) ")
        # get connection
        db_connection = sql_get_connection()
        # compute query
        query = f"SELECT {','.join(field_list)} FROM {table_name} WHERE {sql_fields[object_name]['table_pkey']} = '{object_id}'"
        with db_connection.cursor() as cursor:
            # get entry
            cursor.execute(query)
            result = cursor.fetchone()
            if result:
            # convert all values to string
                result = [str(x) if x else '' for x in result ]
                # convert result to dict
                result_dict = dict(zip(field_list, result))
            else:
                return None
    except Exception as e:
        exit_error(f'[{current_context}] unexpected error while querying existing {object_name}-data from {table_name}: {e}')
    else:
        loguru.logger.success(f"[{current_context}] item found: {result}")
    # return result_dict
    return result_dict


# -----------------------------------------------------------------------------
# database: set field value (write to database)
# -----------------------------------------------------------------------------
def sql_set_field(object_name:str, object_id: int, field_value:str) -> None:
    """ save new value of defined field to database """
    # skip unsupported objects
    if object_name not in sql_fields.keys():
        exit_error(f"get_existing_items: unsupported object_name: {object_name}")
    # get options from sql_fields definitions
    table_name = sql_fields[object_name]['table_name']
    field_name = sql_fields[object_name]['field_name']
    # get entry from database
    current_context = f"sql_set_field.{object_name}.{object_id}.{field_name}"
    try:
        loguru.logger.info(f"[{current_context}] write value into table {table_name}) ")
        loguru.logger.debug(field_value)
        # get connection
        db_connection = sql_get_connection()
        # compute query
        query = f"UPDATE {table_name} SET {field_name}='{field_value}' WHERE {sql_fields[object_name]['table_pkey']} = '{object_id}'"
        with db_connection.cursor() as cursor:
            # run query & commit db
            cursor.execute(query)
            db_connection.commit()
    except Exception as e:
        exit_error(f'[{current_context}] unexpected error while saving data to {object_name}-data from {table_name}: {e}')
    else:
        loguru.logger.success(f"[{current_context}] new value have been saved to database")


# ------------------------------------------------------------------------------
# bootle: cors handling
# ------------------------------------------------------------------------------
@bottle.hook('after_request')
def enable_cors():
    """
    You need to add some headers to each request.
    Don't use the wildcard '*' for Access-Control-Allow-Origin in production.
    """
    bottle.response.headers['Access-Control-Allow-Origin']  = '*'
    bottle.response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    bottle.response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'


# ------------------------------------------------------------------------------
# bootle: landing page
# ------------------------------------------------------------------------------
@bottle.route(f"{app_settings['context_root']}/", method='GET')
def root() -> str:
    return render_html_template(object_name='', object_id='', status_class='warning', status_message=f"{_translate('Ungültiger Aufruf. Wenden Sie sich an den Administrator!')}")


# ------------------------------------------------------------------------------
# bootle: show object's defined field
# ------------------------------------------------------------------------------
@bottle.route(f"{app_settings['context_root']}/<object_name>/<object_id>", method='GET')
def get_note(object_name:str, object_id:str) -> str:
    """ show/edit object's defined field """
    # get related field infos
    try:
        field_name = sql_fields[object_name]['field_name']
        field_infos = sql_get_field(object_name=object_name, object_id=object_id)
        # on empty infos, return error
        if not field_infos:
            return html_error(f"{_translate('Keine passende Daten gefunden für')} {object_name}/{object_id}")
    except KeyError as e:
            return render_html_template(object_name=object_name, object_id=object_id, status_class='danger', status_message=f"{_translate('Objekt wird nicht unterstützt')}: {e}")
    field_value = field_infos[field_name]
    loguru.logger.info(f"old {field_name} for {object_name} id={object_id}: {field_value}")
    return render_html_template(object_name=object_name, object_id=object_id, field_name=field_name, field_value=field_value, field_infos=field_infos)



# ------------------------------------------------------------------------------
# bootle: edit object's defined field
# ------------------------------------------------------------------------------
@bottle.route(f"{app_settings['context_root']}/<object_name>/<object_id>", method='POST')
def set_note(object_name:str, object_id:str) -> str:
    # get related field infos
    try:
        field_name = sql_fields[object_name]['field_name']
    except KeyError as e:
            return render_html_template(object_name=object_name, object_id=object_id, status_class='danger', status_message=f"{_translate('Objekt wird nicht unterstützt')}: {e}")
    # get new field value
    field_value = bottle.request.forms.get(field_name)
    # save new field value to database
    loguru.logger.info(f"saving new {field_name}-value for {object_name} id={object_id}: {field_value}")
    sql_set_field(object_name=object_name, object_id=object_id, field_value=field_value)
    status_class = 'success'
    status_message = f"{_translate('Änderungen wurden gespeichert')}"
    return render_html_template(object_name=object_name, object_id=object_id, field_name=field_name, field_value=field_value, status_class=status_class, status_message=status_message)


# ------------------------------------------------------------------------------
# bootle: show app settings
# ------------------------------------------------------------------------------
@bottle.route(f"{app_settings['context_root']}/api/configs", method='GET')
def get_configs():
    """ configs page: return current app settings as json"""
    return app_settings_safe



# -------------------------------
# execute only if called directly
# -------------------------------
if __name__ == "__main__":

    # start webservice
    if app_settings['dev_mode']:
        loguru.logger.info(f"running in dev-mode")
        loguru.logger.debug(f"app_settings: {app_settings_safe}")
        bottle.run(host=app_settings['app_host'], port=app_settings['app_port'], reloader=True, debug=True)
    else:
        bottle.run(host=app_settings['app_host'], port=app_settings['app_port'])


# eof