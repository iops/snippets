#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
side-application for managing otrs hwk entries
"""
VERSION = '0.0.1'

# import required standard modules
import os, re, sys
from datetime import datetime
from dataclasses import dataclass
from types import ModuleType
from typing import Dict, List, Optional

# import custom modules (must be installed using pip)
import bottle               # web framework
import loguru               # logging framework


# -- script autovars -----------------------------------------------------------
scriptSelf      = sys.argv[0]
scriptArgs      = sys.argv[1:]
scriptName      = os.path.basename(scriptSelf)
scriptBaseName  = os.path.splitext(scriptName)[0]
scriptCallDir   = os.path.dirname(scriptSelf)
scriptFullDir   = os.path.abspath(scriptCallDir)
scriptFullPath  = os.path.abspath(scriptSelf)
scriptTopDir    = os.path.dirname(scriptFullDir)
# -- /script autovars ----------------------------------------------------------


# ------------------------------------------------------------------------------
# app: main settings (overwriteable by environment variables)
# ------------------------------------------------------------------------------
app_settings = {
    # application runtime properties
    'app_name': os.environ.get('APP_NAME', scriptBaseName),
    'app_host': os.environ.get('APP_HOST', 'localhost'),
    'app_port': os.environ.get('APP_PORT', 8000),
    'app_version': os.environ.get('APP_VERSION', VERSION),
    'context_root': os.environ.get('CONTEXT_ROOT', ''),
    # database connection settings (defaults to local db)
    'db_host': os.environ.get('DB_HOST', f"{scriptFullDir}/{scriptBaseName}.db"),
    'db_port': os.environ.get('DB_PORT', ''),
    'db_user': os.environ.get('DB_USER', ''),
    'db_pass': os.environ.get('DB_PASS', ''),
    'db_name': os.environ.get('DB_NAME', ''),
    'db_type': os.environ.get('DB_TYPE', 'sqlite'),
    # development settings
    'dev_mode': os.environ.get('DEV_MODE', False),
}
# safe version without sensitive data (for displaying purposes)
app_settings_safe = { key:value if (not re.search(r'(key|token|pass)', key, re.IGNORECASE)) else '***' for (key,value) in app_settings.items() }
# setup development-specific settings
if app_settings['dev_mode']:
    from icecream import ic
    ic(app_settings)



# ------------------------------------------------------------------------------
# helpers: exit with an error message
# ------------------------------------------------------------------------------
def exit_error(msg: str, exit_code: int = 1):
    """
        exit application with a custom error message
    """    
    loguru.logger.critical(msg)
    sys.exit(exit_code)


# ------------------------------------------------------------------------------
# helpers: return an html error message
# ------------------------------------------------------------------------------
def html_error(msg: str, status_class:str = 'danger'):
    """
        show an html custom error message
    """    
    return render_html_template(object_name='', object_id='', status_class=status_class, status_message=msg)


# ------------------------------------------------------------------------------
# helpers: string translation
# ------------------------------------------------------------------------------
def _translate(msg:str) -> str:
    """
        string translation wrapper
    """
    # does nothing at the moment
    return msg


# ------------------------------------------------------------------------------
# helpers: render html template
# ------------------------------------------------------------------------------
def render_html_template(
    # page options (optional)
    page_title:str =        app_settings['app_name'],
    page_description:str =  None,
    # status messages (optional)
    status_class:str =      'secondary',
    status_message:str =    None,
    # path to the html template file (optional, computed from filename by default)
    template_path:str =     None, 
    ) -> str:
    """
        render html template using provided parameters
    """
    # compute optional values if not set
    # ==================================
    # template path
    if not template_path:
        template_path = __file__.replace('.py','.html')
    # page description
    if not page_description:
        page_description = f"{app_settings['app_name']} - {app_settings['app_version']}"
    # get list of existing files if required
    # ======================================
    # compute template vars
    # =====================
    template_vars = {
        '_translate':               _translate,
        'app_settings':             app_settings,
        'page_title':               page_title,
        'page_description':         page_description,
        'status_class':             status_class,
        'status_message':           status_message,
    }
    # render template 
    # ===============
    return bottle.template(template_path, **template_vars)




# -----------------------------------------------------------------------------
# database: setup sql database connection
# -----------------------------------------------------------------------------
def sql_get_connection(db_settings: dict = None) -> object:
    """
        setup a database connection using the provided settings.
        - returns the connection object after a successful connection
        - exits on any error
        @param db_settings: the database settings
    """

    # use local settings by default
    # =============================
    if not db_settings:
        db_settings = {
            'db_type': app_settings['db_type'],
            'db_host': app_settings['db_host'],
            'db_port': app_settings['db_port'],
            'db_user': app_settings['db_user'],
            'db_pass': app_settings['db_pass'],
            'db_name': app_settings['db_name'],
        }

    # validate settings
    # =================
    for (key,value) in db_settings.items():
        if not value:
            exit_error(f"MISSING DB SETTING: {key}", exit_code=1)
    # set current context
    current_context = f"sql.{db_settings['db_type']}@{db_settings['db_host']}:{db_settings['db_port']}"

    # SQLITE
    # ======
    if db_settings['db_type'] in ['sqlite','sqlite3']:
        db_file_path = f"{db_settings['db_host']}"
        loguru.logger.info(f"[{current_context}] connecting to database")
        try:
            # import the driver
            import sqlite3
            # create a connection to the database
            db_connection = sqlite3.connect(db_file_path)
            # set sqlite3.Row as row_factory
            db_connection.row_factory = sqlite3.Row
        except Exception as e:
            exit_error(f'[{current_context}] failed to connect to database: {e}')
        else:
            loguru.logger.success(f"[{current_context}] connected to {db_settings['db_type']}-database, version: {db_connection.sqlite_version}")
            return db_connection

    # ORACLE
    # ======
    if db_settings['db_type'] in ['oracle']:
        db_connection_string = f"{db_settings['db_host']}:{db_settings['db_port']}/{db_settings['db_name']}"
        loguru.logger.info(f"[{current_context}] connecting to database")
        try:
            # import the driver
            import oracledb
            # create a connection to the database
            db_connection = oracledb.connect(
                user=db_settings['db_user'], 
                password=db_settings['db_pass'], 
                dsn=db_connection_string
            )
        except Exception as e:
            exit_error(f'[{current_context}] failed to connect to database: {e}')
        else:
            loguru.logger.success(f"[{current_context}] connected to {db_settings['db_type']}-database, version: {db_connection.version}")
            return db_connection

    # POSTGRESQL
    # ==========
    if db_settings['db_type'] in ['postgres','postgresql']:
        db_connection_string = f"{db_settings['db_host']}:{db_settings['db_port']}/{db_settings['db_name']}"
        loguru.logger.info(f"[{current_context}] connecting to database")
        try:
            # import the driver
            import psycopg2
            # create a connection to the database
            db_connection = psycopg2.connect(
                user=db_settings['db_user'], 
                password=db_settings['db_pass'], 
                host=db_settings['db_host'], 
                port=db_settings['db_port'], 
                database=db_settings['db_name'], 
            )
        except Exception as e:
            exit_error(f'[{current_context}] failed to connect to database: {e}')
        else:
            loguru.logger.success(f"[{current_context}] connected to {db_settings['db_type']}-database, version: {db_connection.server_version}")
            return db_connection

    # UNSUPPORTED
    # ===========
    else:
        exit_error(f'[{current_context}] Unsupported database type {db_settings["db_type"]}')


# ------------------------------------------------------------------------------
# bootle: cors handling
# ------------------------------------------------------------------------------
@bottle.hook('after_request')
def enable_cors():
    """
    You need to add some headers to each request.
    Don't use the wildcard '*' for Access-Control-Allow-Origin in production.
    """
    bottle.response.headers['Access-Control-Allow-Origin']  = '*'
    bottle.response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    bottle.response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'


# ------------------------------------------------------------------------------
# bootle: landing page
# ------------------------------------------------------------------------------
@bottle.route(f"{app_settings['context_root']}/", method='GET')
def root() -> str:
    return render_html_template(status_class='warning', status_message=f"{_translate('Ungültiger Aufruf. Wenden Sie sich an den Administrator!')}")


# ------------------------------------------------------------------------------
# bootle: show app settings
# ------------------------------------------------------------------------------
@bottle.route(f"{app_settings['context_root']}/api/configs", method='GET')
def get_configs():
    """ configs page: return current app settings as json"""
    return app_settings_safe



# -------------------------------
# execute only if called directly
# -------------------------------
if __name__ == "__main__":

    # # start webservice
    # if app_settings['dev_mode']:
    #     loguru.logger.info(f"running in dev-mode")
    #     loguru.logger.debug(f"app_settings: {app_settings_safe}")
    #     bottle.run(host=app_settings['app_host'], port=app_settings['app_port'], reloader=True, debug=True)
    # else:
    #     bottle.run(host=app_settings['app_host'], port=app_settings['app_port'])

    # test db connection
    db_connection = sql_get_connection()

# eof