#!/bin/bash
# PURPOSE: simple wrapper for fetching/installing project requirements


# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables


# SCRIPT USAGE
printUsage() {
cat <<tac
$(echo;echo)
+--------------------------+
| SCRIPT USAGE             |
+--------------------------+
  ${scriptName} [DEPENDENCIES_PATH(default=.)]
  $(echo)
tac
}


# compute DEPENDENCIES_PATH
DEPENDENCIES_PATH="${1:-.}"
if [[ "${DEPENDENCIES_PATH}" = "-h" ]] || [[ "${DEPENDENCIES_PATH}" = "--help" ]];then
  printUsage
  exit 0
fi


# ------------------------------------------------------------------------------
# include local-dev environment files if exists
# ------------------------------------------------------------------------------
if [[ -f ${scriptFullDir}/requirements.dev ]];then
  echo "[>] importing local environment file"
  . ${scriptFullDir}/requirements.dev
fi


# switch to project toplevel path
cd ${scriptFullDir}


# exit on errors
set -e


# ------------------------------------------------------------------------------
# SETUP ANSIBLE SSH KEY
# ------------------------------------------------------------------------------
ansible_key_file=${scriptFullDir}/ansible.key
ansible_pub_file=${scriptFullDir}/ansible.pub
# create file if envvar is defined
if [[ -n "${ANSIBLE_KEY}" ]];then
  echo "[>] creating ssh key from env: ${ansible_key_file}"
  echo "${ANSIBLE_KEY}"|base64 -d >${ansible_key_file}
  chmod 600 ${ansible_key_file}
fi


# ------------------------------------------------------------------------------
# FETCHING EXTERNAL ROLES
# ------------------------------------------------------------------------------
[[ -d ${DEPENDENCIES_PATH} ]] || mkdir -p ${DEPENDENCIES_PATH}
if [[ -f requirements.yml ]];then
  echo "[>] replacing env vars from requirements.yml"
  envsubst <requirements.yml >${DEPENDENCIES_PATH}/requirements_parsed.yml
  echo "[>] fetching project dependencies into ${DEPENDENCIES_PATH}/"
  ansible-galaxy install --force -r ${DEPENDENCIES_PATH}/requirements_parsed.yml --roles-path ${DEPENDENCIES_PATH} && rm -fv ${DEPENDENCIES_PATH}/requirements_parsed.yml
  echo "[>] remove metafiles from shared ansible-* dirs"
  find */ansible-*    -maxdepth 1 -type d -name "meta"|xargs rm -rfv
  find */ansible-*    -maxdepth 1 -type f -name ".gitignore"|xargs rm -rfv
  find */ansible-*    -maxdepth 1 -type f -name "README*"|xargs rm -rfv
  find */ansible-*    -maxdepth 1 -type f -name "requirements*"|xargs rm -rfv
  echo "[>] remove metafiles from shared gitlab-files dirs"
  find */gitlab-files -maxdepth 1 -type d -name "meta"|xargs rm -rfv
  find */gitlab-files -maxdepth 1 -type f -name ".gitignore"|xargs rm -rfv
  find */gitlab-files -maxdepth 1 -type f -name "README*"|xargs rm -rfv
  find */gitlab-files -maxdepth 1 -type f -name "requirements*"|xargs rm -rfv
  echo "[>] remove metafiles from shared server-* dirs"
  find server-*       -maxdepth 1 -type d -name "meta"|xargs rm -rfv
  find server-*       -maxdepth 1 -type f -name ".gitignore"|xargs rm -rfv
  find server-*       -maxdepth 1 -type f -name "README*"|xargs rm -rfv
  find server-*       -maxdepth 1 -type f -name "requirements*"|xargs rm -rfv
fi


echo "[i] PROJECT [$(basename ${scriptFullDir})]'s requirements has been setup successfully"
# eof
