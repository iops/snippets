#!/bin/sh
# {{ ansible_managed }}
PURPOSE="generic jdk setup script (download and unpack archive)"

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables


# -- usage
printUsage() {
  echo "[i] PURPOSE: ${PURPOSE}"
  echo "[?] USAGE: ${scriptName} --src-url <REMOTE_TGZ_URL> --dst-dir <LOCAL_DST_DIR>"
  echo "[?] CURRENT RECOMMENDED SETTINGS:"
  echo "--"
  echo "${scriptFullPath} --src-url 'https://pkg.iops.at/oracle/jdk/jdk-8u212-linux-x64.tar.gz' --dst-dir /opt/jdk"
  echo "--"
  exit ${1:-0}
}


# -- args parsing
# no params
[ -z "$1" ] && printUsage
# other params
while [ -n "$1" ]
do
  case "${1}" in
    help|--help|-h)
      printUsage
    ;;
    --src-url)
      shift
      JDK_SRC_URL="${1}"
    ;;
    --dst-dir)
      shift
      JDK_DST_DIR="${1}"
    ;;
  esac
  shift
done


# check & validate prerequisites
if [ -z "${JDK_SRC_URL}" ];then
    echo "[x] INVALID ARGUMENT: JDK_SRC_URL"
    printUsage 1
fi
if [ -z "${JDK_DST_DIR}" ];then
    echo "[x] INVALID ARGUMENT: JDK_DST_DIR"
    printUsage 1
fi
echo "[i] installing FROM ${JDK_SRC_URL} TO ${JDK_DST_DIR}"


# compute runtime vars
if basename ${JDK_SRC_URL}|grep -q "ops-pkg";then
  JDK_DIR_NAME=$(basename ${JDK_SRC_URL}|sed "s/.*jdk-1.\([0-9]\).\([0-9]*\)-.*/jdk1.\1.0_\2/")
else
  JDK_DIR_NAME=$(basename ${JDK_SRC_URL}|sed "s/.*jdk-\([0-9]\)u\([0-9]*\)-.*/jdk1.\1.0_\2/")
fi
JDK_DST_PATH="${JDK_DST_DIR}/${JDK_DIR_NAME}"
TGZ_TEMP="${AEM_PATH_TEMP}/$(basename ${JDK_SRC_URL})"


# check if already installed
if [ -x ${JDK_DST_PATH}/bin/java ];then
  echo "[i] using existing installation ${JDK_DST_PATH}"
else

  # download archive
  if [ -f ${TGZ_TEMP} ];then
    echo "[i] using existing archive ${TGZ_TEMP}"
  else
    echo "[i] downloading archive to ${TGZ_TEMP}"
    curl -sfkLo ${TGZ_TEMP} "${JDK_SRC_URL}"
  fi

  # unpack archive
  echo "[i] extracting archive to ${JDK_DST_PATH}"
  [ -d ${JDK_DST_DIR} ] || mkdir -p ${JDK_DST_DIR}
  tar xzf ${TGZ_TEMP} -C ${JDK_DST_DIR}
fi


# configure instance
echo "[i] setting securerandom.source to file:/dev/./urandom"
sed -i "s#securerandom.source=file:/dev/random#securerandom.source=file:/dev/./urandom#" ${JDK_DST_PATH}/jre/lib/security/java.security

# display java infos
echo "[i] JAVA_HOME=${JDK_DST_PATH}"
echo "[i] JAVA VERSION"
echo "--"
${JDK_DST_PATH}/bin/java -version
echo "--"


# eof
