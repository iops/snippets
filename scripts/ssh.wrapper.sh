#!/bin/bash
#  generic wrapper script for ssh connections

# path variables
scriptSelf=$0
scriptName=${scriptSelf##*/}
scriptCallDir=${scriptSelf%/*};cd $scriptCallDir
scriptFullDir=$PWD
scriptFullPath=$scriptFullDir/$scriptName

# parse ssh settings from filename
SSH_TYPE=$(awk -F"." '{print $1}' <<< "$scriptName")
SSH_HOST=$(awk -F"." '{print $2}' <<< "$scriptName")
SSH_PORT=$(awk -F"." '{print $3}' <<< "$scriptName")
SSH_USER=$(awk -F"." '{print $4}' <<< "$scriptName")

# set default options
DEFAULT_DOMAIN=""
DEFAULT_HOST="$scriptName"
DEFAULT_USER=root
DEFAULT_PORT=22
DEFAULT_KEY="~/.ssh/id_rsa"
DEFAULT_OPTS="-C"


# helper functions
check_connectivity() {
   if ! nc -w3 -z $SSH_HOST $SSH_PORT;then
     echo "[x] ERROR: $SSH_HOST:$SSH_PORT is NOT reacheable... $@"
     exit 127
   fi
}

# check type specific settings
case "$SSH_TYPE" in
  # via local tunnel
  sgjenkins01*)
    SSH_USER="support"
    SSH_HOST="localhost"
    SSH_PORT="22001"
    check_connectivity "Please check tunnel settings."
  ;;
  # all other sg hosts
  sg*)
    DEFAULT_DOMAIN=".scigames.at"
  ;;
  # default behaviour
  *)
    SSH_HOST="$DEFAULT_HOST"
    SSH_PORT="$DEFAULT_PORT"
  ;;
esac


# computing final ssh settings
[[ -z "$DOMAIN" ]] && DOMAIN=$DEFAULT_DOMAIN
[[ -z "$SSH_HOST" ]] && SSH_HOST=$DEFAULT_HOST
[[ -z "$SSH_PORT" ]] && SSH_PORT=$DEFAULT_PORT
[[ -z "$SSH_USER" ]] && SSH_USER=$DEFAULT_USER
[[ -z "$SSH_OPTS" ]] && SSH_OPTS=$DEFAULT_OPTS
[[ -z "$SSH_KEY" ]]  && SSH_KEY=$DEFAULT_KEY
echo "[>] connecting to ${SSH_USER}@${SSH_HOST}${DOMAIN}:${SSH_PORT} using key [${SSH_KEY}]"
check_connectivity

# run ssh command
ssh "$SSH_OPTS" -o StrictHostKeyChecking=no -o NoHostAuthenticationForLocalhost=yes -i ${SSH_KEY} -p ${SSH_PORT} -l ${SSH_USER} "${SSH_HOST}${DOMAIN}" $@

