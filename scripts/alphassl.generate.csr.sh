#!/bin/bash
PURPOSE="generate CSR for alpha SSL certificates"


# -- auto: path variables
scriptSelf=$0;
scriptName=${scriptSelf##*/}; scriptCallDir=${scriptSelf%/*};
cd $scriptCallDir; scriptFullDir=$PWD; scriptFullPath=$scriptFullDir/$scriptName;
scriptTopDir=${scriptFullDir%/*}
# -- /auto: path variables


# -- helper functions
# - default output functions
function printVersion { echo -e "\033[1m# $scriptName version $VERSION\033[0m\n"; [[ -n "$1" ]] && exit ${1}; }
function printOutput  { [[ -e $outFile ]] && out=$(cat $outFile); [[ -n "$out" ]] && echo -e "$out" || echo -e "NO OUTPUT"; }
function printTitle   { echo; msg="$*";linesno=$(( ${#msg} + 4 ));msglines=$(for i in `seq $linesno`;do echo -n "=";done);echo -e "$msglines\n# $msg #\n$msglines"; }
function printOut     { echo -en "$@"; }
function printOk      { echo -en "\033[32m$*\033[0m"; }
function printInfo    { echo -en "\033[34m$*\033[0m"; }
function printError   { echo -en "\033[31m$*\033[0m"; }
# - default exit functions
function exitError    { echo -e "\n\033[31m[$HOSTNAME] ERROR: ${1:-"Unknown Error"}. Exiting...\033[0m" 1>&2; exit ${2:-1}; }
function exitInfo     { echo -e "\n\033[34m[$HOSTNAME] INFO: ${1:-"Thank you for using $scriptName"}.\033[0m" 1>&2; exit ${2:-1}; }
# -- /helper functions


# -- script usage
# check NO_PROMPT option
NO_PROMPT=0
[[ -n "$1" && "$1" = "---no-prompt" ]] && NO_PROMPT=1 && shift
# print usage if no parameter
if [[ -z "$1" || "$1" = "-h" || "$1" = "--help" ]];then
  printInfo "\n# $PURPOSE\n"
  printInfo "  Usage: $scriptName [---no-prompt] <[*.]domain.name> [domain.name1 domain.nameN]\n\n"
  exit 0
fi
# -- /script usage


# -- script options

# list of domains to process
DOMAINS="${*}"

# Cert Subject & Infos
company="Capital Bay GmbH"
organisation="CB IT MANAGEMENT GMBH"
location="/C=DE/ST=Berlin/L=Berlin"

# CRT type
CRT_TYPE="rsa:2048"
# -- /script options


# -- print infos
cat <<tac
[i] generating CSR for the following domains: ${DOMAINS}
[i] using the following certificate informations:
--
company: ${company}
organisation: ${organisation}
location: ${location}
--
tac

# -- get user prompt
if [[ ${NO_PROMPT} -ne 1 ]];then
  echo -e 'Press ENTER to continue, CTRL+C to abort!'
  read a
fi


# -- generate CSRs
for domain in ${DOMAINS}
do
  echo -e "\n\n[DOMAIN: ${domain}]"

  # name
  if [[ ${domain} =~ ^\*\. ]];then
    CRT_NAME=$(sed "s/*/wildcard/" <<<${domain})
  else
    CRT_NAME="${domain}"
  fi

  # subject
  CRT_SUBJECT="${location}/O=${company}/OU=${organisation}/CN=${domain}"

  # CSR output
  OUT_DEST_CSR="${scriptTopDir}/crt"; [[ -d ${OUT_DEST_CSR} ]] || mkdir -p ${OUT_DEST_CSR}
  OUT_FILE_CSR="${OUT_DEST_CSR}/${CRT_NAME}.csr"

  # KEY output
  OUT_DEST_KEY="${scriptTopDir}/crt"; [[ -d ${OUT_DEST_KEY} ]] || mkdir -p ${OUT_DEST_KEY}
  OUT_FILE_KEY="${OUT_DEST_KEY}/${CRT_NAME}.key"

  # generate new key and csr
  if [[ -e ${OUT_FILE_KEY} ]];then
    echo -e "[i] EXISTING files for ${domain} found (KEY=${OUT_FILE_KEY}), skipping creation..."
  else
    echo -e "[>] generating NEW files for ${domain} (KEY=${OUT_FILE_KEY}, CSR=${OUT_FILE_CSR})"
    openssl req -new -newkey ${CRT_TYPE} -nodes -out ${OUT_FILE_CSR} -keyout ${OUT_FILE_KEY} -subj "${CRT_SUBJECT}"
  fi

  # display csr
  if [[ -e ${OUT_FILE_CSR} ]];then
    echo -e "[i] here is the content of the created CSR"
    cat ${OUT_FILE_CSR}
  fi
done


exit 0
# eof
