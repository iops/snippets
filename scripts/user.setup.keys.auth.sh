#!/bin/sh
PURPOSE="setup remote user for ssh key auth (must be run as the user itself)"

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables


# -- usage
case "${1}" in
  help|--help|-h)
    echo "[i] PURPOSE: ${PURPOSE}"
    echo "[?] USAGE: ${scriptName}"
    exit 0
  ;;
esac
# -- /usage


# user settings
user_name="$(whoami)"
user_home="${HOME}"
# authorized_keys
read -d '' user_authorized_keys <<daer
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDU50scYKr9RLDx99d7r490qcqyvV8LvExIXI0l7VH0tM+1EHseRxolWwUfR2Z1iTJE38g3I+rzu/nzDtFdP48tI794V1+DCeS0iysPC3stttqKORBIZ5bQ6b8FwGss8olwrT0F5+Wq3jTAUE1PAB9qT/vVLE9vZ1BkonPwZmsQ7P8mJuw8XJqQ6YPJsPuJlHKjKjzSc4sSIJCARPdOXy8RDI0mltByR6ckA9SNj5J4n/nN6A90F82fSo1zYPKz/EUiVs+s72hdh3BFwIAY8Q6EsGmDx4vKClwYvW+e8G+pkgD6m0w5yFwj3ldbfBQSOkdNUIieBDVWZ42DltoRIb2T fidy@sgosbuild1.scigames.at
daer


# setup ssh authorized_keys
if [ -n "${user_authorized_keys}" ];then
  [ -d ${user_home}/.ssh ] || mkdir -p ${user_home}/.ssh
  #chmod 700 ${user_home}/.ssh
  grep -q "${user_authorized_keys}" ${user_home}/.ssh/authorized_keys || echo "${user_authorized_keys}" >>${user_home}/.ssh/authorized_keys
  chmod 600 ${user_home}/.ssh/authorized_keys
  #chown -R ${user_name}: ${user_home}/.ssh
  #chcon -R unconfined_u:object_r:ssh_home_t:s0 ${user_home}/.ssh
fi


# disable account & password expiration
#echo "[>] disabling account/password expiration"
#chage -m 0 -M 99999 -I -1 -E -1 ${user_name}

# eof
