#!/bin/sh
# PURPOSE: install docker-ee on ubuntu LTS
# USAGE: curl -Lks https://gitlab.com/iops/snippets/raw/master/scripts/lts.install.docker.ee.sh | sh

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables

# helper vars
DOCKER_EE_URL="https://storebits.docker.com/ee/ubuntu/sub-f8a5e98e-fe99-4557-8c26-29439fb5b536"
DOCKER_EE_VERSION="stable-17.06"

# exit on error(s)
set -e

# update OS
apt-get update
apt-get dist-upgrade -y

# setup docker-ee repo
curl -kfsSL "${DOCKER_EE_URL}/ubuntu/gpg" | apt-key add -
add-apt-repository "deb [arch=amd64] ${DOCKER_EE_URL}/ubuntu $(lsb_release -cs) ${DOCKER_EE_VERSION}"

# install docker-ee
apt-get update
apt-get install -y docker-ee

# testing docker engine
docker run hello-world

# show daemon infos
docker info

# enable docker service
systemctl enable docker

# eof
