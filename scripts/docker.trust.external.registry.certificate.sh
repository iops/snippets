#!/bin/sh
# {{ ansible_managed }}
PURPOSE="setup an external registry's certificate to be trusted by docker host & daemon"

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables


# -- usage
printUsage() {
  echo "[i] PURPOSE: ${PURPOSE}"
  echo "[?] USAGE: ${scriptName} --registry-url <REGISTRY_URL>"
  echo "[?] EXAMPLE:"
  echo "--"
  echo "${scriptFullPath} --registry-url 'my.external.registry:5001'"
  echo "--"
  exit ${1:-0}
}


# -- args parsing
# no params
[ -z "$1" ] && printUsage
# other params
while [ -n "$1" ]
do
  case "${1}" in
    help|--help|-h)
      printUsage
    ;;
    --registry-url)
      shift
      REGISTRY_URL="${1}"
    ;;
  esac
  shift
done


# check & validate prerequisites
if [ -z "${REGISTRY_URL}" ];then
    echo "[x] MISSING ARGUMENT: REGISTRY_URL"
    printUsage 1
fi


# compute runtime vars
REGISTRY_HOST=$(awk -F ':' '{print $1}' <<<"${REGISTRY_URL}")
REGISTRY_PORT=$(awk -F ':' '{print $2}' <<<"${REGISTRY_URL}")
PKI_CERT_PATH=/etc/pki/tls/${REGISTRY_HOST}.pem
PKI_TRUST_PATH=/etc/pki/ca-trust/source/anchors/${REGISTRY_HOST}.pem
DOCKER_CERT_DIRPATH=/etc/docker/certs.d/${REGISTRY_HOST}:${REGISTRY_PORT}/${REGISTRY_HOST}.crt
DOCKER_CERT_CRTPATH=${DOCKER_CERT_DIRPATH}/${REGISTRY_HOST}.crt


# download & trust certificate on os level
echo "[>] downloading cert FROM ${REGISTRY_URL} TO ${PKI_CERT_PATH}"
openssl s_client -showcerts -connect ${REGISTRY_URL} </dev/null 2>/dev/null|openssl x509 -outform PEM >${PKI_CERT_PATH}

echo "[>] copying cert TO ${PKI_TRUST_PATH}"
cp -v "${PKI_CERT_PATH}" "${PKI_TRUST_PATH}"

echo "[>] trusting cert on os level"
update-ca-trust

# configure certificate to be used by docker daemon
echo "[>] copying cert TO ${DOCKER_CERT_CRTPATH}"
[ -d "${DOCKER_CERT_DIRPATH}" ] || mkdir -p "${DOCKER_CERT_DIRPATH}"
cp -v "${PKI_CERT_PATH}" "${DOCKER_CERT_CRTPATH}"

echo "[i] test using docker login ${REGISTRY_URL}"

# eof
