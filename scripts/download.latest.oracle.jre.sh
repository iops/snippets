#!/bin/sh
# PURPOSE: download latest available oracle jre archive from vendor repo
# USAGE: curl -Lks https://gitlab.com/iops/snippets/raw/master/scripts/download.latest.oracle.jre.sh | sh

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables


# set default settings
# -- param 1: JRE major version
jre_major_version=${1}
# -- param 2: JRE destination dir
jre_destination_dir=${2:-${scriptFullDir}}
# -- param 3: JRE platform filter
jre_platform_suffix=${3}


# check parameters
case "$1" in
  8)
    jre_overview_url='https://www.oracle.com/technetwork/java/javase/downloads/server-jre8-downloads-2133154.html'
    [ -z ${3} ] && jre_platform_suffix="linux-x64.tar.gz"
  ;;
  10)
    jre_overview_url='https://www.oracle.com/technetwork/java/javase/downloads/sjre10-downloads-4417025.html'
    [ -z ${3} ] && jre_platform_suffix="linux-x64_bin.tar.gz"
  ;;
  *)
    echo "[i] USAGE: ${scriptName} <JRE_MAJOR_VERSION> [DESTINATION_DIR (default=${jre_destination_dir})] [PLATFORM_SUFFIX (default=${jre_platform_suffix})]"
    echo
    exit 1
  ;;
esac


# parse download url
jre_download_url=$(curl --cookie "oraclelicense=accept-securebackup-cookie" -kLs ${jre_overview_url} | grep "${jre_platform_suffix}" | tail -1 | sed -e "s/.*\(https:.*${jre_platform_suffix}\).*/\1/")
[ -z ${jre_download_url} ] && ( echo "[x] could not parse download url from page [${jre_overview_url}]" && exit 2 )
jre_dst_filename=$(basename ${jre_download_url})
[ -z ${jre_dst_filename} ] && ( echo "[x] could not parse destination filename from url [${jre_download_url}]" && exit 3 )
jre_dst_filepath=${jre_destination_dir}/${jre_dst_filename}

# check if destination file already exists
if [ -f ${jre_dst_filepath} ];then
  echo "[i] ${jre_dst_filename} file already found locally (${jre_dst_filepath}), skipping download..."
  exit 0
fi

# ensure destination dir exists
[ -d ${jre_destination_dir} ] || ( mkdir -p ${jre_destination_dir} && echo "[i] directory ${jre_destination_dir} has been created" )

# download jre file
set -e
echo "[i] downloading ${jre_dst_filename} from [${jre_download_url}]"
curl --cookie "oraclelicense=accept-securebackup-cookie" -kLo "${jre_dst_filepath}" "${jre_download_url}"


# eof
