#!/bin/sh
# PURPOSE: download latest available oracle jdk archive from vendor repo
# USAGE: curl -Lks https://gitlab.com/iops/snippets/raw/master/scripts/download.latest.oracle.jdk.sh | sh

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables


# set default settings
# -- param 1: JDK major version
jdk_major_version=${1}
# -- param 2: JDK destination dir
jdk_destination_dir=${2:-${scriptFullDir}}
# -- param 3: JDK platform filter
jdk_platform_suffix=${3}


# check parameters
case "$1" in
  8)
    jdk_overview_url='https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html'
    [ -z ${3} ] && jdk_platform_suffix="linux-x64.tar.gz"
  ;;
  10)
    jdk_overview_url='https://www.oracle.com/technetwork/java/javase/downloads/jdk10-downloads-4416644.html'
    [ -z ${3} ] && jdk_platform_suffix="linux-x64_bin.tar.gz"
  ;;
  11|lts)
    jdk_overview_url='https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html'
    [ -z ${3} ] && jdk_platform_suffix="linux-x64_bin.tar.gz"
  ;;
  *)
    echo "[i] USAGE: ${scriptName} <JDK_MAJOR_VERSION> [DESTINATION_DIR (default=${jdk_destination_dir})] [PLATFORM_SUFFIX (default=${jdk_platform_suffix})]"
    echo
    exit 1
  ;;
esac


# parse download url
jdk_download_url=$(curl --cookie "oraclelicense=accept-securebackup-cookie" -kLs ${jdk_overview_url} | grep "${jdk_platform_suffix}" | tail -1 | sed -e "s/.*\(https*:.*${jdk_platform_suffix}\).*/\1/")
[ -z ${jdk_download_url} ] && ( echo "[x] could not parse download url from page [${jdk_overview_url}]" && exit 2 )
jdk_dst_filename=$(basename ${jdk_download_url})
[ -z ${jdk_dst_filename} ] && ( echo "[x] could not parse destination filename from url [${jdk_download_url}]" && exit 3 )
jdk_dst_filepath=${jdk_destination_dir}/${jdk_dst_filename}

# check if destination file already exists
if [ -f ${jdk_dst_filepath} ];then
  echo "[i] ${jdk_dst_filename} file already found locally (${jdk_dst_filepath}), skipping download..."
  exit 0
fi

# ensure destination dir exists
[ -d ${jdk_destination_dir} ] || ( mkdir -p ${jdk_destination_dir} && echo "[i] directory ${jdk_destination_dir} has been created" )

# download jdk file
set -e
echo "[i] downloading ${jdk_dst_filename} from [${jdk_download_url}]"
curl --cookie "oraclelicense=accept-securebackup-cookie" -kLo "${jdk_dst_filepath}" "${jdk_download_url}"


# eof
