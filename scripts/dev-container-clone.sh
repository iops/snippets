#!/usr/bin/env bash
PURPOSE="(re)create a dev-container from an existing one"
VERSION=2022-09-01-001


# -- auto: path variables -----------------------------------------------------
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables ----------------------------------------------------


# -----------------------------------------------------------------------------
# script helpers (available to subscripts when exported)
# -----------------------------------------------------------------------------

# global helpers
# ==============
set -a # enable auto-export first
# output helpers
__is_debug()      { [[ -n ${DEBUG} ]] || [[ -n ${VERBOSE} ]]; }
__echo_debug()    { if __is_debug;then echo -e "\033[35m[${scriptName}.DEBUG]: ${@}\033[0m";fi; }
__echo_error()    { echo -e "\033[31m[${scriptName}.ERROR]:    ${@}\033[0m"; }
__echo_info()     { echo -e "\033[36m[${scriptName}.INFO]:     ${@}\033[0m"; }
__echo_success()  { echo -e "\033[32m[${scriptName}.SUCCESS]:  ${@}\033[0m"; }
__echo_warning()  { echo -e "\033[33m[${scriptName}.WARNING]:  ${@}\033[0m"; }
__exit_error()    { echo -e "\033[41m[${scriptName}.CRITICAL]: ${1}\033[0m"; exit ${2:-1}; }
# path helpers
__path_create()   { this_path="${1}"; this_type="${2}"; if [[ -e ${this_path} ]];then __echo_info "path already exists: ${this_path}";else if [[ "${this_type}" == "-d" ]];then mkdir -p ${this_path};else touch ${this_path};fi;__echo_success "path created: ${this_path}";fi; }
__path_delete()   { this_path="${1}"; if [[ -e ${this_path} ]];then rm -rf ${this_path} && __echo_success "path deleted: ${this_path}";else __echo_info "path does not exist: ${this_path}";fi; }
# print helpers
__print_title()   { echo '*';echo '*'; this_msg="${*}"; this_linesno=$(( ${#this_msg} + 4 )); this_msglines=$(for i in `seq ${this_linesno}`;do echo -n "=";done);echo -e "${this_msglines}\n# ${this_msg} #\n${this_msglines}"; }
__print_env_var() { this_var="${1}"; this_value="${!1}"; echo "[${scriptName}.var] ${this_var}=${this_value}"; }
__print_pwd_var() { this_var="${1}"; this_value="${!1}"; this_out=${this_value::3}$(sed 's/./*/g' <<<"${this_value:2}"); echo "[${scriptName}.var] ${this_var}=${this_out}"; }
__print_varlist() { list_var="${1}"; for this_var in ${!list_var};do this_value="${!this_var}"; echo "[${scriptName}.${list_var}.${this_var}] ${this_var}: ${this_value}";done | column -t; }
__print_version() { echo -e "# ${scriptName}\n# ${PURPOSE}\n# VERSION: ${VERSION}"; }
# requirement checkers
__require_bin()   { this_bin="${1}"; if this_location="$(type -a ${this_bin} 2>/dev/null)";then __echo_debug "required binary found: ${this_location}";else __exit_error "required executable [${this_bin}] not found";fi; }
__require_usr()   { this_usr="${1}"; current_usr=$(id -un); if [[ "${this_usr}" == "${current_usr}" ]];then __echo_debug "running with required user [${this_usr}]";else __exit_error "running with wrong user [${current_usr}] (required is [${this_usr}])";fi; }
__require_var()   { this_var="${1}"; this_value="${!1}"; if [[ "${this_value}" == "" ]];then __exit_error "required variable [${this_var}] is not set";else __echo_debug "required variable set: ${this_var} is ${this_value}";THIS_SCRIPT_OPTIONS+=" ${this_var}";fi; }
set +a # disable auto-export again

# local helpers
# =============
# cleanup tasks
this_cleanup_tasks() {
  __print_title "running cleanup tasks"
  # <put cleanup tasks here>
  echo "[${scriptName}.cleanup] end of cleanup tasks"
}
# error handling functions
this_on_errors() { 
  rc=${?}
  lineno=${1}
  case "${rc}" in
    *)
      this_cleanup_tasks
      __exit_error "UNEXPECTED ERROR (rc=${rc}) occured on line \033[33m [${lineno}] \033[0m" ${rc}
    ;;
  esac
}
# enable error handling
set -e # fail on any error
trap 'this_on_errors ${LINENO}' ERR   # register handlers




# -----------------------------------------------------------------------------
# custom script options (overridable by envvars)
# -----------------------------------------------------------------------------
set -a # enable auto-export first

# name of the source container to clone
THIS_SCRIPT_OPTIONS+=" SRC_CONTAINER_NAME"; if [[ -z ${SRC_CONTAINER_NAME} ]];then SRC_CONTAINER_NAME=dev-default;fi

set +a # disable auto-export again


# -----------------------------------------------------------------------------
# custom script usage
# -----------------------------------------------------------------------------
printUsage() {
__print_version
cat <<tac
USAGE: ${scriptName} [options] <DST_CONTAINER_NAME>
OPTIONS (as switches):
  -f, --force                         recreate destination container if it already exists
  -d, --debug                         enable debug output
  -h, --help                          print this help message
  -v, --version                       print version information

$( (echo "envvars: default-values:";for this_var in ${THIS_SCRIPT_OPTIONS};do echo "${this_var} ${!this_var}";done) | column -t )
  
tac
[[ -n ${1} ]] && exit ${1}
}


# -----------------------------------------------------------------------------
# custom args parsing
# -----------------------------------------------------------------------------
# exit with usage if no params are given
[ -z "$1" ] && printUsage 0
# parse switches & args
THIS_SCRIPT_ARGS=""
while [ -n "$1" ]
do
  case "${1}" in
    # switches
    --force|-f)
      FORCE=1
    ;;
    --debug|-d)
      DEBUG=1
    ;;
    --help|-h)
      printUsage 0
    ;;
    --version|-v)
      __print_version
      exit 0
    ;;
    # args
    *)
      DST_CONTAINER_NAME="${1}"
    ;;
  esac
  shift
done


# -----------------------------------------------------------------------------
# script main logic
# -----------------------------------------------------------------------------

# show script version
__print_version

# show additional infos
if __is_debug;then
__print_title "script context informations"
  __print_env_var SRC_CONTAINER_NAME
  __print_env_var DST_CONTAINER_NAME
  __print_varlist THIS_SCRIPT_OPTIONS
fi

# check requirements: user
__require_usr root
# check requirements: binaries
__require_bin lxc
__require_bin nginx



# check if container exists
# =========================
dst_container_infos=$(lxc info ${DST_CONTAINER_NAME} 2>/dev/null || echo FAILED)
if [[ ${dst_container_infos} == "FAILED" ]];then
  __echo_info "destination container [${DST_CONTAINER_NAME}] will be CREATED from [${SRC_CONTAINER_NAME}]"
  ACTION=CREATE
else
  if [[ ${FORCE} -eq 1 ]];then
    __echo_warning "destination container [${DST_CONTAINER_NAME}] will be RECREATED from [${SRC_CONTAINER_NAME}]"
    ACTION=RECREATE
    # deleting existing container
    __echo_info "deleting existing container [${DST_CONTAINER_NAME}]"
    lxc delete ${DST_CONTAINER_NAME} --force
  else
    __exit_error "destination container [${DST_CONTAINER_NAME}] already exists, use --force to RECREATE it"
  fi
fi


# copy source container to destination container
# ==============================================
__print_title "${ACTION} container [${DST_CONTAINER_NAME}]"
# create container
lxc copy --instance-only ${SRC_CONTAINER_NAME} ${DST_CONTAINER_NAME}
# start container
lxc start ${DST_CONTAINER_NAME}
# wait for container to be ready (get IP address)
retries=0
while true;do
  retries=$(( retries + 1 ))
  dst_container_ip=$(lxc exec ${DST_CONTAINER_NAME} -- ip addr show eth0 | grep -oP '(?<=inet\s)\d+(\.\d+){3}' || echo FAILED)
  if [[ ${dst_container_ip} != "FAILED" ]];then
    break
  elif [[ ${retries} -gt 10 ]];then
    __exit_error "could not get container IP address after 10 retries"
  fi
  sleep 1
done


# (re)create nginx config
# =======================
__print_title "${ACTION} nginx configuration"
# get src container ip
src_container_ip=$(lxc exec ${SRC_CONTAINER_NAME} -- ip addr show eth0 | grep -oP '(?<=inet\s)\d+(\.\d+){3}')
# create nginx config
sed -e "s#${SRC_CONTAINER_NAME}#${DST_CONTAINER_NAME}#g" -e "s#${src_container_ip}#${dst_container_ip}#" /etc/nginx/conf.d/${SRC_CONTAINER_NAME}.conf >/etc/nginx/conf.d/${DST_CONTAINER_NAME}.conf
# test nginx config
nginx -t || __exit_error "nginx configtest failed"
# reload nginx config
service nginx reload || __exit_error "could not reload nginx"


# display final status
# ====================
__echo_success "container [${DST_CONTAINER_NAME}] successfully ${ACTION}D"
echo -e "The following services are available on container [${DST_CONTAINER_NAME}]:\n--"
grep server_name /etc/nginx/conf.d/${DST_CONTAINER_NAME}.conf | sed -e "s#.*server_name *#http://#" -e "s#;\$##"


# eof
