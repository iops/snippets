#!/bin/bash
PURPOSE="generate final certificate chains (intermediate+cert in PEM & PFX formats) for alpha SSL certificates"


# -- auto: path variables
scriptSelf=$0;
scriptName=${scriptSelf##*/}; scriptCallDir=${scriptSelf%/*};
cd $scriptCallDir; scriptFullDir=$PWD; scriptFullPath=$scriptFullDir/$scriptName;
scriptTopDir=${scriptFullDir%/*}
# -- /auto: path variables


# -- helper functions
# - default output functions
function printVersion { echo -e "\033[1m# $scriptName version $VERSION\033[0m\n"; [[ -n "$1" ]] && exit ${1}; }
function printOutput  { [[ -e $outFile ]] && out=$(cat $outFile); [[ -n "$out" ]] && echo -e "$out" || echo -e "NO OUTPUT"; }
function printTitle   { echo; msg="$*";linesno=$(( ${#msg} + 4 ));msglines=$(for i in `seq $linesno`;do echo -n "=";done);echo -e "$msglines\n# $msg #\n$msglines"; }
function printOut     { echo -en "$@"; }
function printOk      { echo -en "\033[32m$*\033[0m"; }
function printInfo    { echo -en "\033[34m$*\033[0m"; }
function printError   { echo -en "\033[31m$*\033[0m"; }
# - default exit functions
function exitError    { echo -e "\n\033[31m[$HOSTNAME] ERROR: ${1:-"Unknown Error"}. Exiting...\033[0m" 1>&2; exit ${2:-1}; }
function exitInfo     { echo -e "\n\033[34m[$HOSTNAME] INFO: ${1:-"Thank you for using $scriptName"}.\033[0m" 1>&2; exit ${2:-1}; }
# -- /helper functions


# -- script usage
# check force option
FORCE=0
[[ -n "$1" && "$1" = "--force" ]] && FORCE=1 && shift
# print usage if no parameter
if [[ -z "$1" || "$1" = "-h" || "$1" = "--help" ]];then
  printInfo "\n# $PURPOSE\n"
  printInfo "  Usage: $scriptName [--force] <[*.]domain.name> [domain.name1 domain.nameN]\n\n"
  exit 0
fi
# -- /script usage


# -- script options

# list of domains to process
DOMAINS="${*}"

# path to root CA certificate file
CRT_ROOTCA_FILE="${scriptTopDir}/crt/alphassl.rootca.crt"
if [[ ! -e ${CRT_ROOTCA_FILE} ]];then
  echo "[x] root certificate file not found (${CRT_ROOTCA_FILE}), aborting..."
  echo "[i] you can get the root-cert from https://www.alphassl.com/support/install-root-certificate.html"
  exit 1
fi

# path to intermediate certificate file
CRT_INTERMEDIATE_FILE="${scriptTopDir}/crt/alphassl.intermediate.crt"
if [[ ! -e ${CRT_INTERMEDIATE_FILE} ]];then
  echo "[x] intermediate certificate file not found (${CRT_INTERMEDIATE_FILE}), aborting..."
  echo "[i] you can get the root-cert from https://www.alphassl.com/support/install-root-certificate.html"
  exit 1
fi


# -- print infos
cat <<tac
[i] generating final certificates for the following domains: ${DOMAINS}
[i] using the following intermediate certificate file: ${CRT_INTERMEDIATE_FILE}
tac


# -- generate certificates
for domain in ${DOMAINS}
do
  echo -e "\n\n[DOMAIN: ${domain}]"

  # name
  if [[ ${domain} =~ ^\*\. ]];then
    CRT_NAME=$(sed "s/*/wildcard/" <<<${domain})
  else
    CRT_NAME="${domain}"
  fi

  # check if base certificate exists
  CRT_BASE_FILE="${scriptTopDir}/crt/${CRT_NAME}.crt"
  if [[ ! -e ${CRT_BASE_FILE} ]];then
    echo "[x] base certificate file not found (${CRT_BASE_FILE}), aborting..."
    echo "[i] you usually get the cert from the order email"
    exit 1
  fi

  # check if base certificate key exists
  KEY_BASE_FILE="${scriptTopDir}/crt/${CRT_NAME}.key"
  if [[ ! -e ${KEY_BASE_FILE} ]];then
    echo "[x] base certificate key file not found (${KEY_BASE_FILE}), aborting..."
    echo "[i] the key file is generally created by the alphassl.generate.csr.sh script"
    exit 1
  fi

  # PEM output
  OUT_DEST_PEM="${scriptTopDir}/out"; [[ -d ${OUT_DEST_PEM} ]] || mkdir -p ${OUT_DEST_PEM}
  OUT_FILE_PEM="${OUT_DEST_PEM}/${CRT_NAME}.pem"
  if [[ -e ${OUT_FILE_PEM} ]];then
    echo -e "[i] EXISTING file for ${domain} found (PEM=${OUT_FILE_PEM}), skipping creation..."
  else
    echo -e "[>] generating PEM file for ${domain} (PEM=${OUT_FILE_PEM})"
    cat ${CRT_BASE_FILE} ${CRT_INTERMEDIATE_FILE} >${OUT_FILE_PEM}
    echo -e "[>] exporting KEY file for ${domain}"
    cp -v ${KEY_BASE_FILE} ${scriptTopDir}/out/
  fi

  # PFX output
  OUT_DEST_PFX="${scriptTopDir}/out"; [[ -d ${OUT_DEST_PFX} ]] || mkdir -p ${OUT_DEST_PFX}
  OUT_FILE_PFX="${OUT_DEST_PFX}/${CRT_NAME}.pfx"
  if [[ -e ${OUT_FILE_PFX} ]];then
    echo -e "[i] EXISTING file for ${domain} found (PFX=${OUT_FILE_PFX}), skipping creation..."
  else
    echo -en "[>] generating one-time PFX export password: "
    OUT_PASS_PFX=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 13) && echo "${OUT_PASS_PFX}" || echo "UNEXPECTED_ERROR"
    echo -e "[>] generating PFX file for ${domain} (PFX=${OUT_FILE_PFX})"
    openssl pkcs12 -export -out ${OUT_FILE_PFX} -inkey ${KEY_BASE_FILE} -in ${CRT_BASE_FILE} -certfile ${CRT_INTERMEDIATE_FILE} -password "pass:${OUT_PASS_PFX}"
  fi

done


exit 0
# eof
