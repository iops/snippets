#!/bin/bash
# PURPOSE: simple wrapper for creating a git tag
#  * NOTE: this script is intended only to run as a gitlab-ci job (runtime variables needed)

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables


# include gitlab environment files (from current project)
[[ -f ${CI_PROJECT_DIR}/.gitlab-ci.dev ]] && . ${CI_PROJECT_DIR}/.gitlab-ci.dev >/dev/null
[[ -f ${CI_PROJECT_DIR}/.gitlab-ci.env ]] && . ${CI_PROJECT_DIR}/.gitlab-ci.env >/dev/null




# CHECK MANDATORY VARS
######################
printUsage() {
cat <<tac
$(echo;echo)
+--------------------------+
| SCRIPT USAGE             |
+--------------------------+
  ${scriptName} <TAG_NAME> <GIT_REF> [TAG_MESSAGE]
  $(echo)
tac
}

# -- exit if CI_PROJECT_ID is not defined
if [[ -z "${CI_PROJECT_ID}" ]];then
  echo -e "(ERROR) required variable [CI_PROJECT_ID] is empty or not defined. Skipping further actions..."
  printUsage
  exit 2
fi

# -- check API requirements
if [[ -z "${API_TOKEN}" ]];then
  echo -e "(ERROR) required variable [API_TOKEN] is empty or not defined. Skipping further actions..."
  exit 2
fi
if [[ -z "${CI_API_V4_URL}" ]];then
  echo -e "(ERROR) required variable [CI_API_V4_URL] is empty or not defined. Skipping further actions..."
  exit 2
fi

# -- exit if TAG_NAME is not defined
[[ -z "${TAG_NAME}" ]] && TAG_NAME=${1}
if [[ -z "${TAG_NAME}" ]];then
  echo -e "(ERROR) required variable [TAG_NAME] is empty or not defined. Skipping further actions..."
  printUsage
  exit 2
fi

# -- exit if GIT_REF is not defined
[[ -z "${GIT_REF}" ]] && GIT_REF=${2}
if [[ -z "${GIT_REF}" ]];then
  echo -e "(ERROR) required variable [GIT_REF] is empty or not defined. Skipping further actions..."
  printUsage
  exit 2
fi

# -- getting TAG_MESSAGE
[[ -z "${TAG_MESSAGE}" ]] && TAG_MESSAGE=${3}
if [[ -z "${TAG_MESSAGE}" ]];then
  TAG_MESSAGE="null"
fi





# CREATING NEW TAG
########################

# -- exit on errors
set -e

# -- display info
cat <<tac
$(echo;echo)
+--------------------------+
| creating a new tag       |
+--------------------------+
  - PROJECT ID:  [${CI_PROJECT_ID}]
  - TAG_NAME:    [${TAG_NAME}]
  - GIT_REF:     [${GIT_REF}]
  - TAG_MESSAGE: [${TAG_MESSAGE}]
tac

# -- create JSON request
JSON_DST_PATH="$(mktemp)"
cat <<tac >${JSON_DST_PATH}
{
  "id": ${CI_PROJECT_ID},
  "ref": "${GIT_REF}",
  "tag_name": "${TAG_NAME}",
  "message": "${TAG_MESSAGE}"
}
tac

# -- deleting tag if already exists
DELETION=$(curl -k -s -i -H "PRIVATE-TOKEN: ${API_TOKEN}" -H "Content-Type: application/json" -X DELETE "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/tags/${TAG_NAME}" 2>/dev/null || true)

# -- send request to gitlab
OUTPUTS=$(curl -k -s -i -H "PRIVATE-TOKEN: ${API_TOKEN}" -H "Content-Type: application/json" -X POST -d "@${JSON_DST_PATH}" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/tags")
rm -f ${JSON_DST_PATH} || true

# -- display results
cat <<tac
$(echo;echo)
+--------------------------+
| tag creation status      |
+--------------------------+
$(grep "^HTTP/"  <<<"${OUTPUTS}" | tail -1)
$(grep "message" <<<"${OUTPUTS}" | tail -1)
tac


exit 0
# eof
