#!/bin/sh
# PURPOSE: install UCP on docker EE host
# USAGE: curl -Lks https://gitlab.com/iops/snippets/raw/master/scripts/docker.ee.install.ucp.sh | sh

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables


# helper vars
DOCKER_EE_UCP_IMAGE="docker/ucp:3.0.5"
DOCKER_EE_UCP_HOSTIP=${1}

# script usage
if [ -z ${DOCKER_EE_UCP_HOSTIP} ];then
  echo "[i] USAGE: ${scriptName} <HOST_IP_ADDRESS>"
  exit 1
fi

# exit on error(s)
set -e

# run install container
docker container run --rm -it --name ucp -v /var/run/docker.sock:/var/run/docker.sock ${DOCKER_EE_UCP_IMAGE} install --interactive  --host-address ${DOCKER_EE_UCP_HOSTIP}


# eof
