# VERSION: 2021-02-17-001

# base image
# ==============================================================================
FROM alpine:3


# build args
# ==============================================================================
ARG BUILD_DIR="/builds"
ARG BUILD_VERSION="latest"
ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1


# add config files
# ==============================================================================
WORKDIR ${BUILD_DIR}
ADD . ${BUILD_DIR}


# setup os
# ==============================================================================
RUN echo "===> Create Version Files..." \
    && echo "${BUILD_VERSION}" > /version \
    && date > /build-date \
    \
    && echo "===> Install OS packages from file..." \
    && apk --update add $(grep -vE "^(#.*|$)" requirements.pkg) \
    \
    && echo "===> Setup PIP build-environment..." \
    && apk --update add --virtual build-dependencies \
      build-base \
      python3-dev \
      libffi-dev \
      openssl-dev \
    && pip3 install --upgrade pip pycrypto cffi \
    \
    && echo "===> Installing PIP packages from file..." \
    && pip3 install -r ${BUILD_DIR}/requirements.txt \
    \
    # && echo "===> Adding additional packages from edge repository..." \
    # && echo "https://dl-cdn.alpinelinux.org/alpine/edge/testing" >>/etc/apk/repositories \
    # && apk --update add packer \
    \
    && echo "===> Installing azure requirements..." \
    && curl -kLo ${BUILD_DIR}/requirements-azure.txt "https://raw.githubusercontent.com/ansible-collections/azure/dev/requirements-azure.txt" \
    && pip3 install -r ${BUILD_DIR}/requirements-azure.txt \
    && ansible-galaxy collection install azure.azcollection \
    \
    && echo "===> Removing PIP build-packages..." \
    && apk del build-dependencies \
    && rm -rf /var/cache/apk/* \
    \
    && echo "===> Adding default ansible hosts for convenience..." \
    && mkdir -p /etc/ansible \
    && echo 'localhost' > /etc/ansible/hosts \
    \
    && echo "===> Testing tools: ansible..." \
    && ansible --version \
    # && echo "===> Testing tools: packer..." \
    # && packer --version \
    && ls -la


# setup final image
# ==============================================================================
LABEL maintainer="Fidy Andrianaivo <fidy@andrianaivo.org>"
LABEL version="${BUILD_VERSION}"

# default envs
ENV ANSIBLE_CONFIG=ansible.cfg
ENV TZ=Europe/Vienna

# set build dir as volume
VOLUME ${BUILD_DIR}

# default command: display Ansible version
CMD [ "ansible-playbook", "--version" ]


# eof
