# PURPOSE: docker file for building el7 base image with systemd enabled
# USAGE: docker build --build-arg RHN_USER=services@apps4you.at --build-arg RHN_PASS=xxxxxxxxx -t docker-tests-rhel7 -f Dockerfile.rhel7 .

# -- base image
FROM registry.access.redhat.com/rhel7/rhel

# -- required args
ARG RHN_USER="services@apps4you.at"
ARG RHN_PASS
ARG RHN_UUID=6202684f-52f5-41ef-8e2d-f1479e6af9cc
ARG RHN_NAME=docker-build
ARG REQUIRED_OS_PACKAGES="ansible cronie openssh-clients openssh-server python-setuptools rsync"
ARG OPTIONAL_OS_PACKAGES
ARG HTTP_PROXY=""
ENV http_proxy=${HTTP_PROXY}
ENV https_proxy=${HTTP_PROXY}

# -- register & subscribe to RHN
# RUN subscription-manager register --username=${RHN_USER} --password=${RHN_PASS} --name=${RHN_NAME} --consumerid=${RHN_UUID}; subscription-manager attach --auto; subscription-manager repos --enable=rhel-7-server-extras-rpms --disable=rhel-7-server-rt-beta-rpms
RUN yum-config-manager --enable rhel-7-server-extras-rpms

# -- install OS PACKAGES and setup sshd
RUN yum -y install ${REQUIRED_OS_PACKAGES} ${OPTIONAL_OS_PACKAGES}; yum clean all; systemctl enable sshd;

# -- setup ssh keys
COPY root root/
RUN chmod 700 /root/.ssh && chmod 600 /root/.ssh/id_ecdsa && chmod 644 /root/.ssh/authorized_keys && chmod 644 /root/.ssh/config

# -- install python pip
RUN curl --silent --show-error --retry 5 https://bootstrap.pypa.io/get-pip.py | python

# -- used ports
EXPOSE 22

# -- enable init
CMD [ "/sbin/init" ]


# eof
