# docker-simulated environment
This is a simulation of a server environment/stage using docker containers.
- **env-specific** settings are stored in `docker-compose.env`
- **list** of corresponding **target servers** are maintained in `docker-compose.yml`


# prerequisites
To run this environment, you will need a host with
- docker engine version 1.12.0+
- docker-compose version 1.9.0+
- enough CPU & RAM resource for the corresponding applications


# working with the environment (ansible use case)
First, make sure that you are working in the **directory** containing the `docker-compose` files (unless you are using the awsome wrapper script `run.sh`)

#### validate your environment definitions
```
# manually
docker-compose config

# using run.sh wrapper
/path/to/env/run.sh status
```

#### check the status of the environment (and its containers)
```
# manually
docker-compose config

# using run.sh wrapper
/path/to/env/run.sh status
```

#### create the environment (start all containers)
```
# manually
docker-compose up -d

# using run.sh wrapper
/path/to/env/run.sh create|start
```

#### login into the ansible container
```
# manually
docker-compose exec ansible bash

# using run.sh wrapper
/path/to/env/run.sh login
```

#### run commands inside the ansible container (without entering)
```
# manually
docker-compose exec ansible <COMMAND> [ARG1] [ARGn]

# using run.sh wrapper
/path/to/env/run.sh <COMMAND> [ARG1] [ARGn]
```

#### stop the environment (stop all containers)
```
# manually
docker-compose stop

# using run.sh wrapper
/path/to/env/run.sh stop
```

#### destroy the environment (stop & delete all containers)
```
# manually
docker-compose down

# using run.sh wrapper
/path/to/env/run.sh delete
```



# troubleshooting the environment
Here are some common use cases for troubleshooting a running environment


#### displaying logs of any or all containers
```
# manually
docker-compose logs [-f] [SERVICE_NAME (server-hostname)]

# using run.sh wrapper
/path/to/env/run.sh [--service SERVICE_NAME] logs [-f]
```

#### login into the any container
```
# manually
docker-compose exec [SERVICE_NAME] bash

# using run.sh wrapper
/path/to/env/run.sh [--service SERVICE_NAME] login
```

#### run commands inside any container (without entering)
```
# manually
docker-compose exec [SERVICE_NAME] <COMMAND> [ARG1] [ARGn]

# using run.sh wrapper
/path/to/env/run.sh [--service SERVICE_NAME] <COMMAND> [ARG1] [ARGn]
```
