#!/bin/bash
PURPOSE="wrapper script for docker-compose environments"

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables


# -- usage
printUsage() {
  echo "[i] PURPOSE: ${PURPOSE}"
  echo "[?] USAGE:"
  echo "[?]   [control]: ${scriptName} [--service SERVICE_NAME] create|start|status|login|stop|delete|logs [-f]"
  echo "[?]   [execute]: ${scriptName} [--service SERVICE_NAME] <COMMAND> [ARG1] [ARGn]"
  echo "[?]   [ansible]: ${scriptName} ansible --version"
  exit ${1:-0}
}


# -- args parsing
ENVIRONMENT_NAME=$(basename ${scriptFullDir})
DEFAULT_SERVICE=ansible
COMPOSE_COMMANDS="default"
COMPOSE_PARAMETERS=""
# no params
[ -z "$1" ] && printUsage
# other params
while [ -n "$1" ]
do
  case "${1}" in
    help|--help|-h)
      printUsage
    ;;
    --service)
      shift
      SERVICE_NAME="${1}"
    ;;
    create|start|status|login|exec|logs|stop|delete)
      COMPOSE_COMMANDS="${1}"
      shift
    ;;
    *)
      COMPOSE_PARAMETERS+=" ${1}"
    ;;
  esac
  shift
done


# -- switch to script dir
pushd ${scriptFullDir} >/dev/null 2>&1


# -- evaluate compose commands
case "${COMPOSE_COMMANDS}" in
  create|start)
    docker-compose up -d ${SERVICE_NAME}
    docker-compose ps
  ;;
  status)
    docker-compose config >/dev/null && echo "[i] SYNTAX OK"
    echo "[i] LIST OF CONTAINERS IN [${ENVIRONMENT_NAME}]"
    docker-compose ps
  ;;
  login)
    docker-compose exec ${SERVICE_NAME:-${DEFAULT_SERVICE}} bash
  ;;
  logs)
    docker-compose logs ${COMPOSE_PARAMETERS} ${SERVICE_NAME}
  ;;
  stop)
    docker-compose stop ${SERVICE_NAME} ${COMPOSE_PARAMETERS}
    docker-compose ps
  ;;
  delete)
    docker-compose down ${COMPOSE_PARAMETERS}
    docker-compose ps
  ;;
  default)
    docker-compose exec ${SERVICE_NAME:-${DEFAULT_SERVICE}} ${COMPOSE_PARAMETERS}
  ;;
esac


# -- switch back to last user dir
popd >/dev/null 2>&1


# eof
